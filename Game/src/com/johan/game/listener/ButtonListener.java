package com.johan.game.listener;

import com.johan.game.objects.Button;

/**
 * Just a ButtonListener when a button is clicked
 * @author Johan
 *
 */
public interface ButtonListener
{
	public void onClick(Button source);
}
