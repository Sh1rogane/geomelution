package com.johan.game.listener;

/**
 * Just a TouchListener for gameObjects to handle touch events
 */
public interface TouchListener
{
	public void onTouchClick();
	public void onTouchOver();
	public void onTouchOut();
}
