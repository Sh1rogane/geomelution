package com.johan.game.pool;

import java.util.LinkedList;

import com.johan.game.objects.Bullet;
import com.johan.game.objects.Enemy;
import com.johan.game.objects.ExpObject;
import com.johan.game.objects.GameObject;

/**
 * Used for object pooling to force less GC to make lagg spikes when playing
 * @author Johan
 *
 */
public class ObjectPool
{
	private static LinkedList<Enemy> enemyPool = new LinkedList<Enemy>();
	private static LinkedList<Bullet> bulletPool = new LinkedList<Bullet>();
	private static LinkedList<ExpObject> expPool = new LinkedList<ExpObject>();
	
	//Creats the pools
	static
	{
		for(int i = 0; i < 50; i++)
		{
			enemyPool.add(new Enemy());
		}
		for(int i = 0; i < 100; i++)
		{
			bulletPool.add(new Bullet());
		}
		for(int i = 0; i < 200; i++)
		{
			expPool.add(new ExpObject());
		}
	}
	//Sets and returns a enemy from the pool
	public static Enemy getEnemy(float x, float y, int lvl)
	{
		Enemy e = enemyPool.poll();
		if(e == null)
		{
			e = new Enemy();
		}
		e.setX(x);
		e.setY(y);
		e.setLevel(lvl);
		return e;
	}
	//Sets and returns a bullet from the pool
	public static Bullet getBullet(float x, float y, float angle, int damage, String owner)
	{
		Bullet b = bulletPool.poll();
		if(b == null)
		{
			b = new Bullet();
		}
		b.setX(x);
		b.setY(y);
		b.setAngle(angle);
		b.setDamage(damage);
		b.setOwner(owner);
		return b;
	}
	//Sets and returns a exp object from the pool
	public static ExpObject getExpObject(float x, float y)
	{
		ExpObject e = expPool.poll();
		if(e == null)
		{
			e = new ExpObject();
		}
		e.reset();
		e.setX(x);
		e.setY(y);
		e.setVx((float)(Math.random() * 6) - 3);
		e.setVy((float)(Math.random() * 6) - 3);
		return e;
	}
	//Puts the game object back to the pool to be resued
	public static void release(GameObject go)
	{
		if(go instanceof Enemy)
		{
			enemyPool.add(Enemy.class.cast(go));
		}
		else if(go instanceof Bullet)
		{
			bulletPool.add(Bullet.class.cast(go));
		}
		else if(go instanceof ExpObject)
		{
			expPool.add(ExpObject.class.cast(go));
		}
	}
}
