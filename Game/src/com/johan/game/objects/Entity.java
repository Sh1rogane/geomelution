package com.johan.game.objects;

/**
 * Base class for the Player, Enemy and boss
 * @author Johan
 *
 */
public class Entity extends GameObject
{
	protected int maxHp;



	protected int hp;


	public Entity()
	{

	}
	public int getMaxHp()
	{
		return maxHp;
	}
	public void setMaxHp(int maxHp)
	{
		this.maxHp = maxHp;
	}
	public int getHp()
	{
		return hp;
	}
	public void setHp(int hp)
	{
		this.hp = hp;
	}
}
