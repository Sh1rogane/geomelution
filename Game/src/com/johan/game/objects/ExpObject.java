package com.johan.game.objects;

import com.johan.game.Graphics;
import com.johan.game.MathUtils;
import com.johan.game.scenes.GameScene;

import android.graphics.Canvas;
import android.graphics.Paint;

/**
 * The exp objects that the player pick up to lvl up
 * @author Johan
 *
 */
public class ExpObject extends GameObject
{
	private boolean attract;
	
	private int speed = 8;
	
	private float vx = 0;
	private float vy = 0;
	
	private int alpha = 255;
	
	private int attractDelay = 30;
	
	public ExpObject()
	{
		this.width = 30;
		this.height = 30;
	}
	public ExpObject(float x, float y)
	{
		this.x = x;
		this.y = y;
		this.width = 30;
		this.height = 30;
	}
	@Override
	public void update()
	{
		attractDelay--;
		//Check the distans, if distance is close enough and magnet is high enough, go towards the player
		float dist = MathUtils.distance(x, y, GameScene.player.x, GameScene.player.y);
		if(dist < GameScene.player.getMagnetLvl() * 100 && attractDelay <= 0)
		{
			attract = true;
		}
		if(attract)
		{
			float angle = MathUtils.angle(x, y, GameScene.player.x + GameScene.player.width / 2, GameScene.player.y + GameScene.player.height / 2);
			vx = (float)Math.cos(Math.toRadians(angle)) * speed;
			vy = (float)Math.sin(Math.toRadians(angle)) * speed;
		}
		else
		{
			vx *= 0.95;
			vy *= 0.95;
		}
		if(attractDelay <= 0)
		{
			alpha--;
		}
		
		this.x += vx;
		this.y += vy;
		if(alpha <= 0)
		{
			removeSelf();
		}
		if(this.intersects(GameScene.player))
		{
			removeSelf();
			GameScene.player.addExp();
		}
	}
	public boolean isAttract()
	{
		return attract;
	}
	public void reset()
	{
		alpha = 255;
		attractDelay = 30;
		attract = false;
		rotation = (float)Math.random() * 360;
	}
	public void setAttract(boolean attract)
	{
		this.attract = attract;
	}
	public float getVx()
	{
		return vx;
	}
	public void setVx(float vx)
	{
		this.vx = vx;
	}
	public float getVy()
	{
		return vy;
	}
	public void setVy(float vy)
	{
		this.vy = vy;
	}
	@Override
	public void render(Canvas canvas)
	{
		int save = canvas.save();

		canvas.translate(x, y);
		canvas.rotate(rotation, width / 2, height / 2);
		p.setAntiAlias(true);
		p.setARGB(alpha, 0, 255, 0);
		p.setStyle(Paint.Style.STROKE);
		p.setStrokeCap(Paint.Cap.ROUND);
		p.setStrokeWidth(5.0f);
		canvas.drawPath(Graphics.getGraphic("exp"), p);
		
		canvas.restoreToCount(save);
		
	}
}
