package com.johan.game.objects;

import com.johan.game.listener.ButtonListener;
import com.johan.game.listener.TouchListener;

import android.graphics.Canvas;

/**
 * Button class for easy use of button in the game
 * @author Johan
 *
 */
public class Button extends GameObject implements TouchListener
{ 
	private String text;
	private boolean onOver;
	private ButtonListener listener;
	
	public Button(float x, float y, int width, int height, String text)
	{
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.text = text;
		this.setTouchListener(this);
	}
	public void setButtonListener(ButtonListener listener)
	{
		this.listener = listener;
	}
	@Override
	public void render(Canvas canvas)
	{
		int save = canvas.save();
		
		canvas.translate(x, y);
		p.setAntiAlias(true);
		if(onOver)
		{
			p.setARGB(255, 150, 150, 150);
		}
		else
		{
			p.setARGB(255, 255, 255, 255);
		}
		
		canvas.drawRect(0, 0, width, height, p);
		p.setARGB(255, 0, 0, 0);
		p.setTextSize(48);
		canvas.drawText(text, width / 2 - p.measureText(text) / 2, 48 + 24, p);
		
		canvas.restoreToCount(save);
	}

	@Override
	public void onTouchClick()
	{
		if(listener != null)
		{
			listener.onClick(this);
		}
	}

	@Override
	public void onTouchOver()
	{
		onOver = true;
	}

	@Override
	public void onTouchOut()
	{
		onOver = false;
	}
}
