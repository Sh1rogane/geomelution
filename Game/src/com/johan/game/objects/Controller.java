package com.johan.game.objects;

import com.johan.game.Input;
import com.johan.game.Input.TouchPointer;
import com.johan.game.MathUtils;

import android.graphics.Canvas;

/**
 * Thumb controller, used for moving and shooting
 * @author Johan
 *
 */
public class Controller extends GameObject
{
	private float touchX = 0;
	private float touchY = 0;
	private boolean onDown = false;
	
	private float moveX = 0;
	private float moveY = 0;
	
	private float powerX = 0;
	private float powerY = 0;
	private float angle = 0;
	
	private int radius = 0;
	
	private int touchId = -1;
	
	public Controller(float x, float y, int radius)
	{
		this.x = x;
		this.y = y;
		this.radius = radius;
		this.width = radius * 2;
		this.height = radius * 2;
	}
	public float getAngle()
	{
		return angle;
	}
	public float getVx()
	{
		return powerX;
	}
	public float getVy()
	{
		return powerY;
	}
	public boolean isActive()
	{
		return onDown;
	}
	@Override
	public void update()
	{
		//Get a pointer and take it
		TouchPointer p = null;
		if(touchId == -1)
		{
			p = Input.getPointer();
		}
		else
		{
			//Log.d("TEST", "id " + touchId);
			p = Input.getPointerAt(touchId);
		}
		if(p != null)
		{
			//Check if pointer is inside of controller
			if(inside(p.getX(), p.getY()))
			{
				onDown = true;
				touchId = p.getId();
				p.inUse();
			}
			//If pointer is down
			if(onDown)
			{
				//get x and y
				touchX = p.getX();
				touchY = p.getY();
				
				//Get the dist between controller center and pointer
				float dist = MathUtils.distance(x + radius, y + radius, touchX, touchY);
				//Get the angle
				angle = MathUtils.angle(x + radius, y + radius, touchX, touchY);
				//If the dist is bigger than the controller radius, limit the controller to move only to the edge
				if(dist > radius)
				{
					float vx = (float) Math.cos(Math.toRadians(angle));
					float vy = (float) Math.sin(Math.toRadians(angle));
					moveX = radius + vx * radius;
					moveY = radius + vy * radius;
				}
				else
				{
					moveX = touchX - x;
					moveY = touchY - y;
				}
				//Calculates a number between 0.0 and 1.0 
				powerX = ((radius - moveX) / radius) * -1;
				powerY = ((radius - moveY) / radius) * -1;
			}
		}
		else
		{
			onDown = false;
			powerX = 0;
			powerY = 0;
			angle = 0;
			touchId = -1;
		}
	}
	@Override
	public void render(Canvas canvas)
	{
		int save = canvas.save();
		
		canvas.translate(x, y);
		p.setAntiAlias(true);
		p.setARGB(150, 255, 255, 255);
		canvas.drawCircle(radius, radius, radius, p);
		
		p.setARGB(150, 0, 0, 0);
		canvas.drawCircle(radius, radius, radius - (radius / 10), p);
		
		p.setARGB(150, 255, 255, 255);
		if(onDown)
		{
			canvas.drawCircle(moveX, moveY, radius / 3, p);
		}
		else
		{
			canvas.drawCircle(radius, radius, radius / 3, p);
		}
		
		canvas.restoreToCount(save);
	}
}
