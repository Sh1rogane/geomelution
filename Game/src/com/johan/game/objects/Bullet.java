package com.johan.game.objects;

import com.johan.game.GameView;
import android.graphics.Canvas;

/**
 * Bullet class for bullets
 * @author Johan
 *
 */
public class Bullet extends GameObject
{
	private float angle;
	private String owner;
	private int damage;
	

	private float vx = 0;
	private float vy = 0;
	private int speed = 10;
	
	public Bullet()
	{
		this.width = 12;
		this.height = 12;
	}
	public Bullet(float x, float y, float angle, int damage, String owner)
	{
		this.x = x - 6;
		this.y = y - 6;
		this.width = 12;
		this.height = 12;
		this.angle = angle;
		this.damage = damage;
		this.owner = owner;

		this.vx = (float)Math.cos(Math.toRadians(angle)) * speed;
		this.vy = (float)Math.sin(Math.toRadians(angle)) * speed;
	}
	
	@Override
	public void update()
	{
		this.x += vx;
		this.y += vy;
		
		if(x > GameView.renderWidth || x < 0 || y > GameView.renderHeight || y < 0)
		{
			removeSelf();
		}
	}
	@Override
	public void render(Canvas canvas)
	{
		int save = canvas.save();
		canvas.translate(x, y);
		p.setAntiAlias(true);
		if(owner.equals("player"))
		{
			p.setARGB(255, 255, 0, 0);
		}
		else
		{
			p.setARGB(255, 255, 255, 0);
		}
		
		canvas.drawCircle(width / 2, width / 2, width / 2, p);
		
		canvas.restoreToCount(save);
	}
	public float getAngle()
	{
		return angle;
	}
	public void setAngle(float angle)
	{
		this.angle = angle;
		this.vx = (float)Math.cos(Math.toRadians(angle)) * (float)speed;
		this.vy = (float)Math.sin(Math.toRadians(angle)) * (float)speed;
	}
	public String getOwner()
	{
		return owner;
	}
	public void setOwner(String owner)
	{
		this.owner = owner;
	}
	public int getDamage()
	{
		return damage;
	}
	public void setDamage(int damage)
	{
		this.damage = damage;
	}
}
