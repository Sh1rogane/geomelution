package com.johan.game.objects;

import android.graphics.Canvas;

import com.johan.game.GameView;
import com.johan.game.listener.TouchListener;
import com.johan.game.scenes.GameScene;

/**
 * A button used for the lvl up
 * @author Johan
 *
 */
public class MutationButton extends GameObject implements TouchListener
{
	private String text;
	private int type;
	private boolean onOver = false;
	
	public MutationButton(float x, float y, String text)
	{
		this.x = x;
		this.y = y;
		this.text = text;
		this.setTouchListener(this);
		this.width = 200;
		this.height = 200;
	}

	public void setType(int type)
	{
		this.type = type;
		switch(type)
		{
			case 0:
				text = "hp+";
				break;
			case 1:
				text = "dmg+";
				break;
			case 2:
				text = "fspd+";
				break;
			case 3:
				text = "nos+";
				break;
			case 4:
				text = "mag+";
				break;
		}
	}
	public int getType()
	{
		return type;
	}
	@Override
	public void render(Canvas canvas)
	{
		int save = canvas.save();
		
		canvas.translate(x, y);
		if(onOver)
		{
			p.setARGB(255, 100, 100, 100);
		}
		else
		{
			p.setARGB(255, 200, 200, 200);
		}
		
		p.setFakeBoldText(true);
		canvas.drawRect(0, 0, 200, 200, p);
		p.setARGB(255, 0, 0, 0);
		p.setTextSize(72);
		canvas.drawText(text, 10, 125, p);
		
		canvas.restoreToCount(save);
	}
	@Override
	public void onTouchClick()
	{
		switch(type)
		{
			case 0:
				GameScene.player.levelUpHp();
				break;
			case 1:
				GameScene.player.levelUpDamage();
				break;
			case 2:
				GameScene.player.levelUpFireSpeed();
				break;
			case 3:
				GameScene.player.levelUpNos();
				break;
			case 4:
				GameScene.player.levelUpMagnet();
				break;
		}
		GameView.setLevelUp(false);
	}

	@Override
	public void onTouchOver()
	{
		onOver = true;
	}

	@Override
	public void onTouchOut()
	{
		onOver = false;
	} 

}
