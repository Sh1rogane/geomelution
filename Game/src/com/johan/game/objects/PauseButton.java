package com.johan.game.objects;

import android.graphics.Canvas;
import com.johan.game.GameView;
import com.johan.game.listener.TouchListener;

/**
 * The pause button used to pause in-game
 * @author Johan
 *
 */
public class PauseButton extends GameObject implements TouchListener
{
	private boolean onOver;
	
	public PauseButton(float x, float y)
	{
		this.x = x;
		this.y = y;
		this.width = 60;
		this.height = 70;
		this.setTouchListener(this);
	}

	@Override
	public void onTouchClick()
	{
		GameView.pause();
	}
	@Override
	public void onTouchOver()
	{
		onOver = true;
	}

	@Override
	public void onTouchOut()
	{
		onOver = false;
	}
	@Override
	public void render(Canvas canvas)
	{
		int save = canvas.save();
		canvas.translate(x, y);
		
		if(onOver)
		{
			p.setARGB(255, 150, 150, 150);
		}
		else
		{
			p.setARGB(255, 255, 255, 255);
		}
		
		canvas.drawRect(0, 0, 20, 60, p);
		canvas.drawRect(30, 0, 50, 60, p);
		
		canvas.restoreToCount(save);
	}

	
	
}
