package com.johan.game.objects;

import com.johan.game.Graphics;
import com.johan.game.MathUtils;
import com.johan.game.VibratorHelper;
import com.johan.game.pool.ObjectPool;
import com.johan.game.scenes.GameScene;
import com.johan.game.scenes.Scene;

import android.graphics.Canvas;
import android.graphics.Paint;

/**
 * The Enemy class that the player must kill
 * @author Johan
 *
 */
public class Enemy extends Entity
{
	private int level = 0;
	private int speed = 2;
	private int damage = 1;
	
	private int fireTime = 0;
	private int fireSpeed = 120;
	
	public Enemy()
	{
		this.width = 60;
		this.height = 60;
		this.hp = 3;
		this.maxHp = hp;
	}
	
	public Enemy(float x, float y, int level)
	{
		this.x = x;
		this.y = y;
		this.width = 60;
		this.height = 60;
		this.level = level;
		this.hp = 3;
		this.maxHp = hp;
	}
	//Sets the enemy player and all variables that are affected by the level
	public void setLevel(int level)
	{
		this.level = level;
		this.maxHp = 3 + (level * 2);
		this.hp = maxHp;
		this.damage = 1 + (int)Math.floor(level / 3);
	}
	@Override
	public void update()
	{
		//Caluclates the velocitys to move towards the player
		float angle = MathUtils.angle(x, y, GameScene.player.getX(), GameScene.player.getY());
		float vx = (float)Math.cos(Math.toRadians(angle)) * speed;
		float vy = (float)Math.sin(Math.toRadians(angle)) * speed;
		
		//If enemy level is 6 or greater, no not move closer that 400 pixels
		if(level >= 6)
		{
			float dist = MathUtils.distance(x, y, GameScene.player.getX(), GameScene.player.getY());
			if(dist < 400)
			{
				this.x -= vx;
				this.y -= vy;
			}
			else
			{
				this.x += vx;
				this.y += vy;
			}
		}
		else
		{
			this.x += vx;
			this.y += vy;
		}
		
		fireTime++;
		
		if(fireTime >= fireSpeed)
		{
			fireTime = 0;
			fire();
		}
		
		//Collision handling
		for(GameObject go : Scene.getScene().getGameObjects())
		{
			if(go instanceof Bullet)
			{
				Bullet b = Bullet.class.cast(go);
				if(b.getOwner().equals("player") && this.intersects(go))
				{
					hp -= b.getDamage();
					go.removeSelf();
					break;
				}
			}
			else if(go instanceof Player)
			{
				if(this.intersects(go))
				{
					removeSelf();
					VibratorHelper.makeVibration(25);
					break;
				}
			}
		}
		if(hp <= 0)
		{
			removeSelf();
			throwExp();
		}
	}
	/**
	 * If lower than lvl 4, fire a bullet towards the player, if between lvl 4 and 7, shoot a bullet towards the player future position
	 * and if higher than 6 shoot 2 bullets
	 */
	private void fire()
	{
		float angle = MathUtils.angle(x, y, GameScene.player.x, GameScene.player.y);
		float dist = MathUtils.distance(x, y, GameScene.player.x, GameScene.player.y) / 10;
		float predictX = GameScene.controller1.getVx() * dist * 5;
		float predictY = GameScene.controller1.getVy() * dist * 5;
		if(level < 4)
		{
			Scene.getScene().addGameObject(ObjectPool.getBullet(x + (width / 2), y + (height / 2), angle, damage, "enemy"));
		}
		else if(level >= 4 && level < 7)
		{
			angle = MathUtils.angle(x, y, GameScene.player.x + predictX, GameScene.player.y + predictY);
			Scene.getScene().addGameObject(ObjectPool.getBullet(x + (width / 2), y + (height / 2), angle, damage, "enemy"));
		}
		else
		{
			Scene.getScene().addGameObject(ObjectPool.getBullet(x + (width / 2), y + (height / 2), angle - 10, damage, "enemy"));
			Scene.getScene().addGameObject(ObjectPool.getBullet(x + (width / 2), y + (height / 2), angle + 10, damage, "enemy"));
		}
	}
	/**
	 * Throws x amount of exp objects
	 */
	private void throwExp()
	{
		for(int i = 0; i < level + 3; i++)
		{
			Scene.getScene().addGameObject(ObjectPool.getExpObject(x + width / 2, y + height / 2));
		}
	}
	@Override
	public void render(Canvas canvas)
	{
		int save = canvas.save();
		canvas.translate(x, y);
		canvas.rotate(rotation, width / 2, height / 2); 
		
		p.setARGB(255, 255, 255, 255);
		p.setStyle(Paint.Style.STROKE);
		p.setStrokeCap(Paint.Cap.ROUND);
		p.setStrokeWidth(7.0f);
		canvas.drawPath(Graphics.getGraphic("lvl" + (level + 1)), p);
		
		p.setARGB(255, 255, 0, 255);
		p.setStyle(Paint.Style.FILL);
		canvas.drawRect(0, -20, (hp * width / maxHp), -10, p);
		
		canvas.restoreToCount(save);
	}
}
