package com.johan.game.objects;

import android.graphics.Canvas;
import android.graphics.Paint;

import com.johan.game.GameView;
import com.johan.game.Graphics;
import com.johan.game.PrefHelper;
import com.johan.game.VibratorHelper;
import com.johan.game.pool.ObjectPool;
import com.johan.game.scenes.LoseScene;
import com.johan.game.scenes.Scene;
import com.johan.game.scenes.GameScene;

/**
 * The player class that the player controlls
 * @author Johan
 *
 */
public class Player extends Entity
{
	private double speed = 5;
	// private boolean fire;

	private int fireSpeed = 20;
	private int fireSpeedLvl = 1;

	private int fireTime = 0;

	private int lvl = 1;
	private int exp = 0;
	private int maxExp = 10;
	private int damageLvl = 1;
	private int nrShots = 1;

	private int magnetLvl = 0;

	public Player(float x, float y)
	{
		this.x = x;
		this.y = y;
		this.width = 60;
		this.height = 60;
		this.hp = 15;
		this.maxHp = 15;
	}

	@Override
	public void update()
	{
		//Checks that the player is inside the play area and moves the player
		if(x + GameScene.controller1.getVx() * speed > 0 && x + width + GameScene.controller1.getVx() * speed < GameView.renderWidth)
		{
			this.x += GameScene.controller1.getVx() * speed;
		}
		if(y + GameScene.controller1.getVy() * speed > 0 && y + height + GameScene.controller1.getVy() * speed < GameView.renderHeight)
		{
			this.y += GameScene.controller1.getVy() * speed;
		}


		rotation += Math.max(Math.abs(GameScene.controller1.getVx() * speed), Math.abs(GameScene.controller1.getVy() * speed));

		fireTime++;

		if(GameScene.controller2.isActive())
		{
			if(fireTime >= fireSpeed)
			{
				fireTime = 0;
				fire();
			}
		}

		if(exp >= maxExp)
		{
			levelUp();
		}
		//Collision handling
		for(GameObject go : Scene.getScene().getGameObjects())
		{
			if(go instanceof Bullet)
			{
				Bullet b = Bullet.class.cast(go);
				if(b.getOwner().equals("enemy") && this.intersects(go))
				{
					hp -= b.getDamage();
					go.removeSelf();
					VibratorHelper.makeVibration(10);
					break;
				}
			}
		}
		
		if(hp <= 0)
		{
			Scene.setScene(new LoseScene());
			PrefHelper ph = PrefHelper.getPrefHelper();
			if(ph.getBestLvl() < lvl)
			{
				ph.setBestLevel(lvl);
			}
		}
	}
	public void levelUpHp()
	{
		maxHp += 5;
		hp = maxHp;
	}
	public void levelUpDamage()
	{
		damageLvl++;
	}
	public void levelUpFireSpeed()
	{
		fireSpeedLvl++;
		fireSpeed = 20 - ((fireSpeedLvl - 1) * 2);
	}
	public void levelUpNos()
	{
		nrShots++;
	}
	public void levelUpMagnet()
	{
		magnetLvl++;
	}
	public void levelUp()
	{
		lvl++;
		exp = 0;
		maxExp = lvl * 10;
		hp = maxHp;
		GameView.setLevelUp(true);
	}
	public int getMaxExp()
	{
		return maxExp;
	}
	public int getExp()
	{
		return exp;
	}
	public int getFireSpeedLvl()
	{
		return fireSpeedLvl;
	}

	public int getDamageLvl()
	{
		return damageLvl;
	}

	public int getNrShots()
	{
		return nrShots;
	}

	public int getMagnetLvl()
	{
		return magnetLvl;
	}
	public int getLevel()
	{
		return lvl;
	}
	public void addExp()
	{
		exp++;
	}
	/**
	 * Fires x amount of bullets depending on nrShots
	 */
	private void fire()
	{
		float angle = GameScene.controller2.getAngle();
		
		if(nrShots == 1)
		{
			Scene.getScene().addGameObject(ObjectPool.getBullet(x + width / 2, y + height / 2, angle, damageLvl, "player"));
		}
		else if(nrShots == 2)
		{
			Scene.getScene().addGameObject(ObjectPool.getBullet(x + width / 2, y + height / 2, angle - 10, damageLvl, "player"));
			Scene.getScene().addGameObject(ObjectPool.getBullet(x + width / 2, y + height / 2, angle + 10, damageLvl, "player"));
		}
		else if(nrShots == 3)
		{
			Scene.getScene().addGameObject(ObjectPool.getBullet(x + width / 2, y + height / 2, angle - 10, damageLvl, "player"));
			Scene.getScene().addGameObject(ObjectPool.getBullet(x + width / 2, y + height / 2, angle + 10, damageLvl, "player"));
			Scene.getScene().addGameObject(ObjectPool.getBullet(x + width / 2, y + height / 2, angle, damageLvl, "player"));
		}
		else if(nrShots == 4)
		{
			Scene.getScene().addGameObject(ObjectPool.getBullet(x + width / 2, y + height / 2, angle - 20, damageLvl, "player"));
			Scene.getScene().addGameObject(ObjectPool.getBullet(x + width / 2, y + height / 2, angle - 10, damageLvl, "player"));
			Scene.getScene().addGameObject(ObjectPool.getBullet(x + width / 2, y + height / 2, angle + 10, damageLvl, "player"));
			Scene.getScene().addGameObject(ObjectPool.getBullet(x + width / 2, y + height / 2, angle + 20, damageLvl, "player"));
		}
		else if(nrShots == 5)
		{
			Scene.getScene().addGameObject(ObjectPool.getBullet(x + width / 2, y + height / 2, angle - 20, damageLvl, "player"));
			Scene.getScene().addGameObject(ObjectPool.getBullet(x + width / 2, y + height / 2, angle - 10, damageLvl, "player"));
			Scene.getScene().addGameObject(ObjectPool.getBullet(x + width / 2, y + height / 2, angle, damageLvl, "player"));
			Scene.getScene().addGameObject(ObjectPool.getBullet(x + width / 2, y + height / 2, angle + 10, damageLvl, "player"));
			Scene.getScene().addGameObject(ObjectPool.getBullet(x + width / 2, y + height / 2, angle + 20, damageLvl, "player"));
		}
	}
	@Override
	public void render(Canvas canvas)
	{
		int save = canvas.save();
		canvas.translate(x, y);
		canvas.rotate(rotation, width / 2, height / 2);

		p.setARGB(255, 0, 255, 0);
		p.setStyle(Paint.Style.STROKE);
		p.setStrokeCap(Paint.Cap.ROUND);
		p.setStrokeWidth(7.0f);
		p.setAntiAlias(true);
		canvas.drawPath(Graphics.getGraphic("lvl" + (lvl)), p);

		canvas.restoreToCount(save);
	}
}
