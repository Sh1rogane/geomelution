package com.johan.game.objects;

import com.johan.game.GameView;
import com.johan.game.MathUtils;
import com.johan.game.pool.ObjectPool;
import com.johan.game.scenes.GameScene;
import com.johan.game.scenes.Scene;
import com.johan.game.scenes.WinScene;

import android.graphics.Canvas;
import android.graphics.Paint;

/**
 * The boss enemy that will appear late game
 * @author Johan
 *
 */
public class Boss extends Entity
{
	private int damage = 5;
	
	private int shootTimer = 0;
	private int attackTimer = 0;
	private int currentAttack = 0;
	
	private int alpha = 255;
	
	public Boss(float x, float y)
	{
		this.x = x;
		this.y = y;
		this.width = 100;
		this.height = 100;
		this.maxHp = 100;
		this.hp = maxHp;
		
		currentAttack = (int)Math.floor(Math.random() * 3);
	}
	
	@Override
	public void update()
	{
		super.update();
		if(currentAttack == 0)
		{
			attackTimer++;
			if(attackTimer > 5 * 60)
			{
				attackTimer = 0;
				currentAttack = (int)Math.floor(Math.random() * 3);
			}
			attack1();
		}
		else if(currentAttack == 1)
		{
			attackTimer++;
			if(attackTimer > 5 * 60)
			{
				attackTimer = 0;
				currentAttack = (int)Math.floor(Math.random() * 3);
			}
			attack2();
		}
		else if(currentAttack == 2)
		{
			attackTimer++;
			
			if(attackTimer > 5 * 120)
			{
				attackTimer = 0;
				this.alpha = 255;
				currentAttack = (int) Math.floor(Math.random() * 3);
			}
			attack3();
		}
		for(GameObject go : Scene.getScene().getGameObjects())
		{
			if(go instanceof Bullet)
			{
				Bullet b = Bullet.class.cast(go);
				if(b.getOwner().equals("player") && this.intersects(go))
				{
					hp -= b.getDamage();
					go.removeSelf();
					break;
				}
			}
		}
		
		if(hp <= 0)
		{
			Scene.setScene(new WinScene());
		}
	}
	/**
	 * Shoots either a bullet to the player or the players future position per 25 tick
	 */
	private void attack1()
	{
		shootTimer++;
		if(shootTimer >= 25)
		{
			shootTimer = 0;
			float predict = MathUtils.distance(GameScene.player.x, x, GameScene.player.y,y) / 10;
			
			float angleShot = 0;
			if(Math.floor(Math.random() * 2) == 0)
			{
				angleShot = (float) MathUtils.angle(x, y, GameScene.player.x + predict * 5 * GameScene.controller1.getVx(), GameScene.player.y + predict * 5 * GameScene.controller1.getVy());
			}
			else
			{
				angleShot = (float) MathUtils.angle(x, y, GameScene.player.x, GameScene.player.y);
			}
			Bullet b = ObjectPool.getBullet(x + width / 2, y + height / 2, angleShot, damage, "enemy");
			Scene.getScene().addGameObject(b);
		}
		
	}
	/**
	 * Shoots a bullet at random each 5 tick
	 */
	private void attack2()
	{
		shootTimer++;
		if(shootTimer >= 6)
		{
			shootTimer = 0;
			float angleShot = (float) Math.random() * 360;
			
			Bullet b = ObjectPool.getBullet(x + width / 2, y + height / 2, angleShot, damage, "enemy");
			Scene.getScene().addGameObject(b);
		}
	}
	//Slowly disapears and pops up atleast 300 pixels from the player and shots  5 bullets in all directions
	private void attack3()
	{
		shootTimer++;
		this.alpha -= 2;
		if(shootTimer >= 120)
		{
			shootTimer = 0;
			this.x = (float) Math.random() * GameView.renderWidth;
			this.y = (float) Math.random() * GameView.renderHeight;
			float dist = MathUtils.distance(GameScene.player.x, x, GameScene.player.y,y);
			
			while(dist < 300)
			{
				this.x = (float) Math.random() * GameView.renderWidth;
				this.y = (float) Math.random() * GameView.renderHeight;
				dist = MathUtils.distance(GameScene.player.x, x, GameScene.player.y, y);
			}
			this.alpha = 255;
			for(int i= 0; i < 5; i++)
			{
				float angleShot = i * 72;
				Bullet b = ObjectPool.getBullet(x + width / 2, y + height / 2, angleShot, damage, "enemy");
				Scene.getScene().addGameObject(b);
			}
		}
	}
	@Override
	public void render(Canvas canvas)
	{
		super.render(canvas);
		int save = canvas.save();
		canvas.translate(x, y);
		p.setAntiAlias(true);
		p.setARGB(alpha, 255, 255, 255);
		
		canvas.drawCircle(width / 2, height / 2, width/2, p);
		p.setARGB(255, 255, 0, 255);
		p.setStyle(Paint.Style.FILL);
		canvas.drawRect(0, -20, (hp * width / maxHp), -10, p);
		canvas.restoreToCount(save);
	}
}
