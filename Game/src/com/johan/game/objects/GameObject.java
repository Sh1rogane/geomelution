package com.johan.game.objects;

import com.johan.game.Input;
import com.johan.game.listener.TouchListener;
import com.johan.game.scenes.Scene;

import android.graphics.Canvas;
import android.graphics.Paint;

/**
 * Base class for every thing that will update and render
 * @author Johan
 *
 */
public abstract class GameObject
{
	protected float x = 0;
	protected float y = 0;
	protected int width = 0;
	protected int height = 0;

	protected float rotation = 0;

	private TouchListener listener;
	private boolean onOver;

	protected Paint p = new Paint();
	
	public void update()
	{
		if(listener != null)
		{
			if(onOver && !Input.isDown())
			{
				listener.onTouchClick();
				onOver = false;
			}
			if(inside(Input.getX(), Input.getY()) && Input.isDown())
			{
				listener.onTouchOver();
				onOver = true;
			}
			else
			{
				listener.onTouchOut();
				onOver = false;
			}
		}
	}
	public void setX(float x)
	{
		this.x = x;
	}
	public void setY(float y)
	{
		this.y = y;
	}
	public void render(Canvas canvas)
	{

	}
	public void setTouchListener(TouchListener listener)
	{
		this.listener = listener;
	}
	/**
	 * Checks if x and y is inside
	 * @param x
	 * @param y
	 * @return
	 */
	public boolean inside(float x, float y)
	{
		if(x > this.x && x < this.x + this.width && y > this.y && y < this.y + this.height)
		{
			return true;
		}

		return false;
	}
	/**
	 * Checks if other is intersecting with this
	 * @param other
	 * @return
	 */
	public boolean intersects(GameObject other)
	{
		if(x + width > other.x && x < other.x + other.width && y + height > other.y && y < other.y + other.height)
		{
			return true;
		}

		return false;
	}
	public void removeSelf()
	{
		Scene.getScene().removeGameObject(this);
	}
	public float getX()
	{
		return x;
	}
	public float getY()
	{
		return y;
	}
}
