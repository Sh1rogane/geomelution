package com.johan.game;

import android.graphics.Canvas;
import android.util.Log;
import android.view.SurfaceHolder;

/**
 * This the the game thread or loop that updates when everything should update and render
 * @author Johan
 *
 */
public class GameThread extends Thread
{
	private final static int MAX_FPS = 100;   
	private final static int MAX_FRAME_SKIPS = 0;    
	private final static int FRAME_PERIOD = 1000 / MAX_FPS;  
	
	private SurfaceHolder surfaceHolder;
	private GameView gameView;
	
	private boolean running;
	
	private static int fps = 0;
	
	public GameThread(SurfaceHolder surfaceHolder, GameView gameView)
	{
		this.surfaceHolder = surfaceHolder;
		this.gameView = gameView;
		this.setPriority(MAX_PRIORITY);
	}
	
	public void setRunning(boolean running)
	{
		this.running = running;
	}
	public boolean isRunning()
	{
		return running;
	}
	public static int getFps()
	{
		return fps;
	}
	/**
	 * This is the main method
	 * It has a while true loop that will run until exit.
	 * It handels everything that should be updated
	 */
	@Override
	public void run()
	{
		Log.d("GAMETHREAD", "Starting loop");
		
		Canvas canvas = null;
		
		long beginTime = 0;
		long timeDiff = 0;
		long sleepTime = 0;
		long framesSkipped = 0;
		
		long time = System.currentTimeMillis();
		int ticks = 0;
		
		while(running)
		{
			canvas = null;
			try
			{
				canvas = surfaceHolder.lockCanvas();
				synchronized(surfaceHolder)
				{
					beginTime = System.currentTimeMillis();
					framesSkipped = 0;
					
					gameView.update();
					ticks++;
					gameView.render(canvas);
					
					timeDiff = System.currentTimeMillis() - beginTime;
					sleepTime = (int) (FRAME_PERIOD - timeDiff);
					
					if(sleepTime > 0)
					{
						try
						{
							Thread.sleep(sleepTime);
						}
						catch(InterruptedException e)
						{
						}
					}
					while(sleepTime < 0 && framesSkipped < MAX_FRAME_SKIPS)
					{
						this.gameView.update();
						sleepTime += FRAME_PERIOD;
						framesSkipped++;
					}
					
					if(time + 1000 <= System.currentTimeMillis())
					{
						time += 1000;
						//Log.d("GAMETHREAD", "Ticks: " + ticks);
						fps = ticks;
						ticks = 0;
					}
				}
			}
			finally
			{
				if(canvas != null)
				{
					surfaceHolder.unlockCanvasAndPost(canvas);
				}
			}
		}
	}

}
