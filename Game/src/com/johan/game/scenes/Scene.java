package com.johan.game.scenes;

import java.util.ArrayList;

import android.graphics.Canvas;

import com.johan.game.objects.Bullet;
import com.johan.game.objects.Enemy;
import com.johan.game.objects.ExpObject;
import com.johan.game.objects.GameObject;
import com.johan.game.pool.ObjectPool;

/**
 * Base class for Scenes. Updates and render all game objects in its list (when it is the current scene)
 * @author Johan
 *
 */
public class Scene
{
	private ArrayList<GameObject> gameObjects = new ArrayList<GameObject>();
	private ArrayList<GameObject> addObjects = new ArrayList<GameObject>();
	private ArrayList<GameObject> removeObjects = new ArrayList<GameObject>();
	
	private static Scene currentScene = new MenuScene();

	public static void setScene(Scene scene)
	{
		currentScene.clean();
		currentScene = scene;
	}
	public static Scene getScene()
	{
		return currentScene;
	}
	private void clean()
	{
		//removeObjects.addAll(gameObjects);
		//addAndRemoveObjects();
	}
	public ArrayList<GameObject> getGameObjects()
	{
		return new ArrayList<GameObject>(gameObjects);
	}
	public void addGameObject(GameObject go)
	{
		addObjects.add(go);
	}
	public void removeGameObject(GameObject go)
	{
		removeObjects.add(go);
	}
	public void update()
	{
		addAndRemoveObjects();
		for(GameObject g : gameObjects)
		{
			g.update();
		}
	}
	public void render(Canvas canvas)
	{
		for(GameObject g : gameObjects)
		{
			g.render(canvas);
		}
	}
	
	private void addAndRemoveObjects()
	{
		if(addObjects.size() > 0)
		{
			gameObjects.addAll(addObjects);
			addObjects.clear();
		}
		if(removeObjects.size() > 0)
		{
			for(GameObject g : removeObjects)
			{
				gameObjects.remove(g);
				if(g instanceof Bullet || g instanceof Enemy || g instanceof ExpObject)
				{
					ObjectPool.release(g);
				}
			}
			
			removeObjects.clear();
		}
	}
}
