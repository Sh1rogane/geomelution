package com.johan.game.scenes;

import android.graphics.Canvas;
import android.graphics.Paint;

import com.johan.game.MusicHelper;
import com.johan.game.PrefHelper;
import com.johan.game.listener.ButtonListener;
import com.johan.game.objects.Button;

/**
 * A win scene saying "YOU WIN!" and a back button
 * @author Johan
 *
 */
public class WinScene extends Scene implements ButtonListener
{
	private Button menu = new Button(490, 500, 300, 100, "Menu");
	
	private Paint p = new Paint();
	
	public WinScene()
	{
		this.addGameObject(menu);
		menu.setButtonListener(this);
		MusicHelper.stopMusic();
		
		PrefHelper ph = PrefHelper.getPrefHelper();
		ph.setBossBeaten(true);
	}

	@Override
	public void onClick(Button source) 
	{
		Scene.setScene(new MenuScene());
	}
	@Override
	public void render(Canvas canvas)
	{
		super.render(canvas);
		
		p.setAntiAlias(true);
		p.setARGB(255, 255, 255, 255);
		p.setFakeBoldText(true);
		p.setTextSize(72);
		
		canvas.drawText("YOU WIN!", 1280 / 2 - p.measureText("YOU WIN!") / 2, 100, p);
		
		p.setTextSize(48);
		canvas.drawText("You won and became the perfect", 300, 200, p);
		canvas.drawText("circle! Congratulation!", 300, 250, p);
		canvas.drawText("There are absolutly nothing", 300, 300, p);
		canvas.drawText("more to this game :D", 300, 350, p);
	}
}
