package com.johan.game.scenes;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PointF;

import com.johan.game.GameView;
import com.johan.game.MusicHelper;
import com.johan.game.objects.Boss;
import com.johan.game.objects.Controller;
import com.johan.game.objects.Enemy;
import com.johan.game.objects.PauseButton;
import com.johan.game.objects.Player;
import com.johan.game.pool.ObjectPool;

/**
 * The Game scene containing all things for the game
 * @author Johan
 *
 */
public class GameScene extends Scene
{
	public static Controller controller1 = new Controller(100, 450, 100);
	public static Controller controller2 = new Controller(980, 450, 100);
	
	public static Player player;
	
	private int spawnTimer = 0;
	private int spawnRate = 180;
	private int bossTimer = 0;
	private int bossTime = 600;
	
	private boolean bossStarted = false;
	
	private Paint p = new Paint();
	private int showTextTime = 0;
	private boolean showText = false;
	
	public GameScene()
	{
		player = new Player(600, 300);
		this.addGameObject(player);
		this.addGameObject(controller1);
		this.addGameObject(controller2);
		this.addGameObject(new PauseButton(20, 20));
		MusicHelper.startMusic();
	}
	
	@Override
	public void update()
	{
		super.update();
		
		handleSpawn();
	}
	
	@Override
	public void render(Canvas canvas)
	{
		super.render(canvas);
		p.setAntiAlias(true);
		//render all text
		renderText(canvas, "hp " + player.getHp() + "/" + player.getMaxHp(), GameView.renderWidth - 150, 100);
		renderText(canvas, "lvl " + player.getLevel(), GameView.renderWidth - 150, 140);
		renderText(canvas, "exp " + player.getExp() + "/" + player.getMaxExp(), GameView.renderWidth - 150, 180);
		renderText(canvas, "dmg " + player.getDamageLvl() + "/5", GameView.renderWidth - 150, 220);
		renderText(canvas, "fspd " + player.getFireSpeedLvl() + "/5", GameView.renderWidth - 150, 260);
		renderText(canvas, "nos " + player.getNrShots() + "/5", GameView.renderWidth - 150, 300);
		renderText(canvas, "mag " + player.getMagnetLvl() + "/3", GameView.renderWidth - 150, 340);
		if(showText)
		{
			p.setTextSize(72);
			p.setFakeBoldText(true);
			canvas.drawText("BOSS INCOMING!", 1280 / 2 - p.measureText("BOSS INCOMING!") / 2, 100, p);
		}
		
	}
	/**
	 * Render the text given at x and y
	 * @param canvas
	 * @param text
	 * @param x
	 * @param y
	 */
	private void renderText(Canvas canvas, String text, float x, float y)
	{
		int save = canvas.save();
		canvas.translate(x, y);
		
		p.setARGB(255, 255, 255, 255);
		p.setTextSize(32);
		p.setFakeBoldText(false);
		canvas.drawText(text, 0, 0, p);
		
		canvas.restoreToCount(save);
	}
	/**
	 * Handles the spawn of enemies depending of some criterias
	 */
	private void handleSpawn()
	{
		spawnTimer++;
		if(spawnTimer > spawnRate && !bossStarted)
		{
			spawnTimer = 0;
			
			PointF point = getRandomSpwan();
			Enemy e = ObjectPool.getEnemy(point.x, point.y, (int) Math.floor(Math.random() * player.getLevel()));
			this.addGameObject(e);
			
			if(player.getLevel() >= 5 && player.getLevel() < 9 && !bossStarted)
			{
				spawnRate = 150;
			}
			else if(player.getLevel() >= 9  && !bossStarted)
			{
				bossTimer++;
				spawnRate = 120;
			}
			if(bossTimer >= bossTime && !bossStarted)
			{
				spawnRate = 150;
				bossStarted = true;
				showText = true;
			}
		}
		if(player.getLevel() >= 9  && !bossStarted)
		{
			bossTimer++;
		}
		
		if(bossStarted && showTextTime  < 180)
		{
			showTextTime++;
			if(showTextTime >= 180)
			{
				startBoss();
			}
		}
	}
	private void startBoss()
	{
		showText = false;
		Boss boss = new Boss(1000, 300);
		this.addGameObject(boss);
	}
	//Returns a point somewhere outside the play area
	private PointF getRandomSpwan()
	{
		PointF point = new PointF();
		int side = (int)Math.floor(Math.random() * 4);
		//Right
		if(side == 0)
		{
			point.x = GameView.renderWidth + 100;
			point.y = (float)Math.random() * GameView.renderHeight;
		}
		//Down
		else if(side == 1)
		{
			point.x = (float)Math.random() * GameView.renderWidth;
			point.y = GameView.renderHeight + 100;
		}
		//Left
		else if(side == 2)
		{
			point.x = -100;
			point.y = (float)Math.random() * GameView.renderHeight;
		}
		//Up
		else if(side == 3)
		{
			point.x = (float)Math.random() * GameView.renderWidth;
			point.y = -100;
		}
		return point;
	}
	@Override
	public String toString()
	{
		return "GameScene";
	}
}
