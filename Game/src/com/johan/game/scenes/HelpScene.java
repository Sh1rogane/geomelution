package com.johan.game.scenes;

import com.johan.game.listener.ButtonListener;
import com.johan.game.objects.Button;

import android.graphics.Canvas;
import android.graphics.Paint;

/**
 * The help scene only showing what all the different words means
 * @author Johan
 *
 */
public class HelpScene extends Scene implements ButtonListener
{
	private Button back = new Button(490, 500, 300, 100, "Back");
	
	private Paint p = new Paint();
	
	public HelpScene()
	{
		this.addGameObject(back);
		back.setButtonListener(this);
	}
	
	@Override
	public void render(Canvas canvas)
	{
		super.render(canvas);
		p.setARGB(255, 255, 255, 255);
		p.setAntiAlias(true);
		
		p.setTextSize(72);
		canvas.drawText("Help", 1280 / 2 - p.measureText("Help") / 2, 100, p);
		p.setTextSize(48);
		canvas.drawText("hp - health", 750, 150, p);
		canvas.drawText("lvl - level", 750, 200, p);
		canvas.drawText("exp - experience", 750, 250, p);
		canvas.drawText("dmg - damage", 750, 300, p);
		canvas.drawText("fspd - fire speed", 750, 350, p);
		canvas.drawText("nos - number of shots", 750, 400, p);
		canvas.drawText("mag - magnet", 750, 450, p);
		
		canvas.drawText("Your goal is to", 100, 150, p);
		canvas.drawText("kill all the enemies", 100, 200, p);
		canvas.drawText("and collect the 'DNA' or exp", 100, 250, p);
		canvas.drawText("to evolve yourself towards", 100, 300, p);
		canvas.drawText("becoming the perfrect circle", 100, 350, p);
		canvas.drawText("(And to win you must beat", 100, 400, p);
		canvas.drawText("the boss at lvl 9)", 100, 450, p);
	}

	@Override
	public void onClick(Button source) 
	{
		Scene.setScene(new MenuScene());
	}
}
