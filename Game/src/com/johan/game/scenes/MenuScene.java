package com.johan.game.scenes;

import android.graphics.Canvas;
import android.graphics.Paint;

import com.johan.game.listener.ButtonListener;
import com.johan.game.objects.Button;

/**
 * The menu scene containing some button to start the game and view help
 * @author Johan
 *
 */
public class MenuScene extends Scene implements ButtonListener
{
	private Button start = new Button(490, 200, 300, 100, "Start");
	private Button help = new Button(490, 350, 300, 100, "Help");
	private Button about = new Button(490, 500, 300, 100, "About");
	
	private Paint p = new Paint();
	
	public MenuScene()
	{
		this.addGameObject(start);
		this.addGameObject(help);
		this.addGameObject(about);
		
		start.setButtonListener(this);
		help.setButtonListener(this);
		about.setButtonListener(this);
	}
	@Override
	public void render(Canvas canvas)
	{
		super.render(canvas);
		
		p.setAntiAlias(true);
		p.setARGB(255, 0, 255, 0);
		p.setFakeBoldText(true);
		p.setTextSize(72);
		
		canvas.drawText("Geomelution", 1280 / 2 - p.measureText("Geomelution") / 2, 100, p);
	}
	
	@Override
	public void onClick(Button source)
	{
		if(source == start)
		{
			Scene.setScene(new GameScene());
		}
		else if(source == help)
		{
			Scene.setScene(new HelpScene());
		}
		else if(source == about)
		{
			Scene.setScene(new AboutScene());
		}
	}
}
