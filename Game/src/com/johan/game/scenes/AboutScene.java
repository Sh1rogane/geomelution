package com.johan.game.scenes;

import android.graphics.Canvas;
import android.graphics.Paint;

import com.johan.game.PrefHelper;
import com.johan.game.listener.ButtonListener;
import com.johan.game.objects.Button;

/**
 * A scene containing some information such as version and best run
 * @author Johan
 *
 */
public class AboutScene extends Scene implements ButtonListener
{
	private Button back = new Button(490, 500, 300, 100, "Back");
	
	private Paint p = new Paint();
	
	public AboutScene()
	{
		this.addGameObject(back);
		back.setButtonListener(this);
	}
	@Override
	public void render(Canvas canvas)
	{
		super.render(canvas);
		
		p.setARGB(255, 255, 255, 255);
		p.setAntiAlias(true);
		p.setTextSize(72);
		p.setFakeBoldText(false);
		p.setUnderlineText(false);
		canvas.drawText("About", 1280 / 2 - p.measureText("About") / 2, 100, p);
		
		p.setTextSize(48);
		canvas.drawText("Version 0.9", 1280 / 2 - p.measureText("Version 0.9") / 2, 200, p);
		canvas.drawText("There might be more updates later", 1280 / 2 - p.measureText("There might be more updates later") / 2, 250, p);
		p.setFakeBoldText(true);
		p.setUnderlineText(true);
		canvas.drawText("Best run", 1280 / 2 - p.measureText("Best run") / 2, 350, p);
		p.setFakeBoldText(false);
		p.setUnderlineText(false);
		PrefHelper ph = PrefHelper.getPrefHelper();
		canvas.drawText("Lvl: " + ph.getBestLvl(), 1280 / 2 - p.measureText("Lvl: " + ph.getBestLvl()) / 2, 400, p);
		if(ph.getBossBeaten())
		{
			canvas.drawText("Boss beaten!", 1280 / 2 - p.measureText("Boss beaten!") / 2, 450, p);
		}
		
	}
	@Override
	public void onClick(Button source) 
	{
		Scene.setScene(new MenuScene());
	}
}
