package com.johan.game.scenes;

import android.graphics.Canvas;
import android.graphics.Paint;

import com.johan.game.MusicHelper;
import com.johan.game.listener.ButtonListener;
import com.johan.game.objects.Button;

/**
 * A scene only saying "YOU LOSE!" and with a back button
 * @author Johan
 *
 */
public class LoseScene extends Scene implements ButtonListener
{
	private Button menu = new Button(490, 500, 300, 100, "Menu");
	
	private Paint p = new Paint();
	
	public LoseScene()
	{
		this.addGameObject(menu);
		menu.setButtonListener(this);
		MusicHelper.stopMusic();
	}

	@Override
	public void onClick(Button source) 
	{
		Scene.setScene(new MenuScene());
	}
	
	@Override
	public void render(Canvas canvas)
	{
		super.render(canvas);
		
		p.setAntiAlias(true);
		p.setARGB(255, 255, 255, 255);
		p.setFakeBoldText(true);
		p.setTextSize(72);
		
		canvas.drawText("YOU LOSE!", 1280 / 2 - p.measureText("YOU LOSE!") / 2, 100, p);
		
		p.setTextSize(48);
		canvas.drawText("You failed to become the perfect", 300, 200, p);
		canvas.drawText("circle! Try again and see if you", 300, 250, p);
		canvas.drawText("can become the perfect circle", 300, 300, p);
	}
}
