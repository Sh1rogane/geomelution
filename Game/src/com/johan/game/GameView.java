package com.johan.game;

import java.util.ArrayList;

import com.johan.game.objects.MutationButton;
import com.johan.game.scenes.GameScene;
import com.johan.game.scenes.Scene;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.media.MediaPlayer;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

/**
 * The surfaceview that will draw everything. Implements SurfaceHolder.Callback for when the surfaceview is created, desstoryd or changed
 * @author Johan
 *
 */
public class GameView extends SurfaceView implements SurfaceHolder.Callback
{
	private static String TAG = "GAMEVIEW";
	private int screenWidth = 0;
	private int screenHeight = 0;
	private GameThread gameThread;

	public final static int renderWidth = 1280;
	public final static int renderHeight = 720;
	public static float leftPadding = 0;
	public static float topPadding = 0;

	private static float scaleX = 1;
	private static float scaleY = 1;
	
	private static boolean pause = false;
	private static boolean levelUp = false;
	
	private static Context c;
	
	private Paint paint = new Paint();
	
	private static MutationButton m1 = new MutationButton(150, 250, "1");
	private static MutationButton m2 = new MutationButton(825, 250, "2");
	
	private static MediaPlayer mediaPlayer;

	public GameView(Context context)
	{
		super(context);
		c = context;
		this.getHolder().addCallback(this);
		this.setFocusable(true);
		gameThread = new GameThread(getHolder(), this);
		
		mediaPlayer = MediaPlayer.create(context, R.raw.music);
	}
	public static MediaPlayer getMediaPlayer()
	{
		return mediaPlayer;
	}
	public static float getScale()
	{
		return (float) Math.min(scaleX, scaleY);
	}
	public static Context context()
	{
		return c;
	}
	@Override
	public void surfaceCreated(SurfaceHolder holder)
	{
		//Get the width and height
		screenWidth = getWidth();
		screenHeight = getHeight();
		//Calculate the x and y scale
		scaleX = (float) screenWidth / renderWidth;
		scaleY = (float) screenHeight / renderHeight;

		Log.d(TAG, "w: " + scaleX);
		Log.d(TAG, "h: " + scaleY);
		
		//Check if thread state is new
		if(gameThread.getState() == Thread.State.NEW)
		{
			gameThread.setRunning(true);
			gameThread.start();
		}
		else
		{
			gameThread = new GameThread(getHolder(), this);
			gameThread.setRunning(true);
			gameThread.start();
		}
		
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width, int height)
	{
		screenWidth = getWidth();
		screenHeight = getHeight();

		scaleX = (float) screenWidth / renderWidth;
		scaleY = (float) screenHeight / renderHeight;
	}
	
	
	@Override
	public void surfaceDestroyed(SurfaceHolder holder)
	{
		Log.d(TAG, "Surface Destroyed");
		boolean retry = true;
		gameThread.setRunning(false);
		while(retry)
		{
			try
			{
				gameThread.join();
				retry = false;
			}
			catch(InterruptedException e)
			{

			}
		}
		pause();
	}
	/**
	 * Call when player level up.
	 * Makes that 2 button shows up with different power ups
	 * @param b
	 */
	public static void setLevelUp(boolean b)
	{
		levelUp = b;
		if(levelUp)
		{
			ArrayList<Integer> forbidden = new ArrayList<Integer>();
			if(GameScene.player.getDamageLvl() == 5)
			{
				forbidden.add(1);
			}
			if(GameScene.player.getFireSpeedLvl() == 5)
			{
				forbidden.add(2);
			}
			if(GameScene.player.getNrShots() == 5)
			{
				forbidden.add(3);
			}
			if(GameScene.player.getMagnetLvl() == 3)
			{
				forbidden.add(4);
			}
			int choice1 = (int)Math.floor(Math.random() * 5);
			int choice2 = (int)Math.floor(Math.random() * 5);
			
			
			while(choice2 == choice1)
			{
				choice2 = (int)Math.floor(Math.random() * 5);
			}
			while(forbidden.contains(choice1))
			{
				choice1 = (int)Math.floor(Math.random() * 5);
			}
			while(forbidden.contains(choice2))
			{
				choice2 = (int)Math.floor(Math.random() * 5);
			}
			
			m1.setType(choice1);
			m2.setType(choice2);
		}
	}
	public static void pause()
	{
		MusicHelper.stopMusic();
		pause = true;
	}
	public static void resume()
	{
		pause = false;
		if(Scene.getScene().toString().equals("GameScene"))
		{
			MusicHelper.resumeMusic();
		}
	}
	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		Input.handleInput(event);
		return true;
	}
	@Override
	protected void onDraw(Canvas canvas)
	{

	}
	/**
	 * Updates everything depending if the game is paused, levelUp or other state
	 */
	public void update()
	{
		if(!pause && !levelUp)
		{
			if(Scene.getScene() != null)
			{
				Scene.getScene().update();
			}
		}
		else if(levelUp)
		{
			m1.update();
			m2.update();
		}
		else
		{
			if(Input.isDown())
			{
				resume();
			}
		}
		
	}
	/**
	 * Here does everything that will be drawn
	 * @param canvas
	 */
	public void render(Canvas canvas)
	{
		if(canvas == null)
		{
			return;
		}
		//The calucation of the scaling and position of the view
		final float scaleFactor = getScale();
		final float finalWidth = renderWidth * scaleFactor;
		final float finalHeight = renderHeight * scaleFactor;
		leftPadding = (screenWidth - finalWidth) / 2.0f;
		topPadding = (screenHeight - finalHeight) / 2.0f;

		canvas.drawColor(Color.GRAY);
		final int savedState = canvas.save();
		try
		{
			//The acutal scaling and positioning
			canvas.clipRect(leftPadding, topPadding, leftPadding + finalWidth, topPadding + finalHeight);
			canvas.translate(leftPadding, topPadding);
			canvas.scale(scaleFactor, scaleFactor);

			canvas.drawColor(Color.BLACK);
			
			if(Scene.getScene() != null)
			{
				Scene.getScene().render(canvas);
			}
			//If pause state, draw a pause window
			if(pause)
			{
				paint.setARGB(150, 0, 0, 0);
				canvas.drawRect(0, 0, renderWidth, renderHeight, paint);
				paint.setARGB(255, 255, 255, 255);
				paint.setTextSize(72);
				canvas.drawText("PAUSED", 500, 300, paint);
				paint.setTextSize(48);
				canvas.drawText("Touch anywhere to unpause", 350, 350, paint);
				
			}
			//If levelUp state draw levelUp window
			if(levelUp)
			{
				paint.setARGB(255, 150, 150, 150);
				paint.setStyle(Paint.Style.FILL);
				canvas.drawRect(100, 50, 1100, 500, paint);
				paint.setARGB(255, 0, 0, 0);
				paint.setTextSize(72);
				canvas.drawText("EVOLUTION LEVEL UP!", 225, 150, paint);
				paint.setTextSize(48);
				canvas.drawText("MUTATION 1", 125, 225, paint);
				canvas.drawText("MUTATION 2", 800, 225, paint);
				paint.setARGB(255, 0, 255, 0);
				paint.setStyle(Paint.Style.STROKE);
				paint.setStrokeCap(Paint.Cap.ROUND);
				paint.setStrokeWidth(7.0f);
				canvas.translate(475, 300);
				canvas.drawPath(Graphics.getGraphic("lvl" + (GameScene.player.getLevel() - 1)), paint);
				canvas.translate(100, 0);
				paint.setARGB(255, 0, 0, 0);
				canvas.drawPath(Graphics.getGraphic("arrow"), paint);
				canvas.translate(100, 0);
				paint.setARGB(255, 0, 255, 0);
				canvas.drawPath(Graphics.getGraphic("lvl" + (GameScene.player.getLevel())), paint);
				canvas.translate(-675, -300);
				m1.render(canvas);
				m2.render(canvas);
			}
			displayFps(canvas, "fps: " + GameThread.getFps());
		}
		finally
		{
			canvas.restoreToCount(savedState);
		}

	}
	/**
	 * Displays the fps in the right corner
	 * @param canvas
	 * @param fps
	 */
	private void displayFps(Canvas canvas, String fps)
	{
		if(canvas != null && fps != null)
		{
			paint.setStyle(Paint.Style.FILL);
			paint.setStrokeWidth(0);
			paint.setARGB(255, 255, 255, 255);
			paint.setTextSize(32);
			canvas.drawText(fps, renderWidth - 150, 60, paint);
		}
	}

}
