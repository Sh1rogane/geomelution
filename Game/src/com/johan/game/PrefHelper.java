package com.johan.game;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * A helper class to eaiser use shared preferences
 * @author Johan
 *
 */
public class PrefHelper
{
	public static final String PREF_NAME = "GeomelutionPref";

	private SharedPreferences sp;
	private SharedPreferences.Editor edit;
	private Context context;
	
	public static PrefHelper getPrefHelper()
	{
		return new PrefHelper(GameView.context());
	}
	
	public PrefHelper(Context context)
	{
		this.context = context;
		sp = this.context.getSharedPreferences(PREF_NAME, 0);
		edit = sp.edit();
	}
	
	public int getBestLvl()
	{
		return sp.getInt("BestLvl", 1);
	}
	public void setBestLevel(int lvl)
	{
		edit.putInt("BestLvl", lvl);
		edit.commit();
	}
	public boolean getBossBeaten()
	{
		return sp.getBoolean("BossBeaten", false);
	}
	public void setBossBeaten(boolean beaten)
	{
		edit.putBoolean("BossBeaten", beaten);
		edit.commit();
	}
}
