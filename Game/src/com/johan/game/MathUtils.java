package com.johan.game;

/**
 * Complementary class to make som math function eaiser
 * @author Johan
 *
 */
public class MathUtils
{
	public static float distance(float x1, float y1, float x2, float y2)
	{
		return (float)Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2));
	}
	
	public static float angle(float x1, float y1, float x2, float y2)
	{
		return (float)Math.toDegrees(Math.atan2(y2 - y1, x2 - x1));
	}
}
