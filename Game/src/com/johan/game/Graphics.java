package com.johan.game;

import android.graphics.Path;

/**
 * Handles almost every graphic that is drawn
 * @author Johan
 *
 */
public class Graphics
{
	//Static Path to draw different shapes
	private static Path arrow = new Path();
	private static Path exp = new Path();
	private static Path lvl1 = new Path();
	private static Path lvl2 = new Path();
	private static Path lvl3 = new Path();
	private static Path lvl4 = new Path();
	private static Path lvl5 = new Path();
	private static Path lvl6 = new Path();
	private static Path lvl7 = new Path();
	private static Path lvl8 = new Path();
	//private static Path lvl9 = new Path();
	
	//Init every path
	static
	{
		arrow.moveTo(0, 30);
		arrow.lineTo(40, 30);
		arrow.lineTo(40, 10);
		arrow.lineTo(60, 30);
		arrow.lineTo(40, 50);
		arrow.lineTo(40, 30);
		
		exp.moveTo(0, 0);
		exp.quadTo(30, 15, 0, 30);
		exp.moveTo(15, 0);
		exp.quadTo(-15, 15, 15, 30);
		
		lvl1.moveTo(0, 30);
		lvl1.lineTo(60, 30);
		
		lvl2.moveTo(0, 30);
		lvl2.lineTo(60, 30);
		lvl2.moveTo(30, 0);
		lvl2.lineTo(30, 60);
		
		lvl3.moveTo(30, 0);
		lvl3.lineTo(60, 60);
		lvl3.lineTo(0, 60);
		lvl3.lineTo(30, 0);
		
		lvl4.moveTo(0, 0);
		lvl4.lineTo(60, 0);
		lvl4.lineTo(60, 60);
		lvl4.lineTo(0, 60);
		lvl4.lineTo(0, 0);
		
		lvl5.moveTo(30, 0);
		lvl5.lineTo(60, 25);
		lvl5.lineTo(50, 60);
		lvl5.lineTo(10, 60);
		lvl5.lineTo(0, 25);
		lvl5.lineTo(30, 0);
		
		lvl6.moveTo(0, 30);
		lvl6.lineTo(15, 0);
		lvl6.lineTo(45, 0);
		lvl6.lineTo(60, 30);
		lvl6.lineTo(45, 60);
		lvl6.lineTo(15, 60);
		lvl6.lineTo(0, 30);
		
		lvl7.moveTo(30, 0);
		lvl7.lineTo(50, 10);
		lvl7.lineTo(60, 35);
		lvl7.lineTo(45, 60);
		lvl7.lineTo(15, 60);
		lvl7.lineTo(0, 35);
		lvl7.lineTo(10, 10);
		lvl7.lineTo(30, 0);
		
		lvl8.moveTo(20, 0);
		lvl8.lineTo(40, 0);
		lvl8.lineTo(60, 20);
		lvl8.lineTo(60, 40);
		lvl8.lineTo(40, 60);
		lvl8.lineTo(20, 60);
		lvl8.lineTo(0, 40);
		lvl8.lineTo(0, 20);
		lvl8.lineTo(20, 0);
	}
	
	/**
	 * Returns the path depending on type
	 * @param type
	 * @return
	 */
	public static Path getGraphic(String type)
	{
		if(type.equals("exp"))
		{
			return exp;
		}
		else if(type.equals("arrow"))
		{
			return arrow;
		}
		else if(type.equals("lvl1"))
		{
			return lvl1;
		}
		else if(type.equals("lvl2"))
		{
			return lvl2;
		}
		else if(type.equals("lvl3"))
		{
			return lvl3;
		}
		else if(type.equals("lvl4"))
		{
			return lvl4;
		}
		else if(type.equals("lvl5"))
		{
			return lvl5;
		}
		else if(type.equals("lvl6"))
		{
			return lvl6;
		}
		else if(type.equals("lvl7"))
		{
			return lvl7;
		}
		else if(type.equals("lvl8"))
		{
			return lvl8;
		}
		return lvl8;
	}
}
