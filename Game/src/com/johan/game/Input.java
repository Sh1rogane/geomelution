package com.johan.game;

import android.util.SparseArray;
import android.view.MotionEvent;

/**
 * Handles every input from GameView and makes it easier to use input else where
 * @author Johan
 *
 */
public class Input
{
	//private static final String TAG = "INPUT";

	private static SparseArray<TouchPointer> activePointers = new SparseArray<TouchPointer>();
	
	private static float x = 0, y = 0;
	private static boolean isDown;

	/**
	 * Handles every input from GameView and makes it easier to use input else where
	 * @param event
	 */
	public static void handleInput(MotionEvent event)
	{
		x = (event.getX() - GameView.leftPadding) / GameView.getScale();
		y = (event.getY() - GameView.topPadding) / GameView.getScale();
		isDown = true;
		int pointerIndex = event.getActionIndex();

		int pointerId = event.getPointerId(pointerIndex);
		
		int maskedAction = event.getActionMasked();

		switch(maskedAction)
		{

			case MotionEvent.ACTION_DOWN:
			case MotionEvent.ACTION_POINTER_DOWN:
			{
				TouchPointer f = new TouchPointer();
				f.id = pointerId;
				f.x = (event.getX(pointerIndex) - GameView.leftPadding) / GameView.getScale();
				f.y = (event.getY(pointerIndex) - GameView.topPadding) / GameView.getScale();
				activePointers.put(pointerId, f);
				break;
			}
			case MotionEvent.ACTION_MOVE:
			{
				for(int size = event.getPointerCount(), i = 0; i < size; i++)
				{
					TouchPointer point = activePointers.get(event.getPointerId(i));
					if(point != null)
					{
						point.x = (event.getX(i) - GameView.leftPadding) / GameView.getScale();
						point.y = (event.getY(i) - GameView.topPadding) / GameView.getScale();
					}
				}
				break;
			}
			case MotionEvent.ACTION_UP:
			case MotionEvent.ACTION_POINTER_UP:
			case MotionEvent.ACTION_CANCEL:
			{
				isDown = false;
				activePointers.remove(pointerId);
				break;
			}
		}
	}
	public static void reset()
	{
		x = -1;
		y = -1;
	}
	/**
	 * Returns a pointer (touch point) that are not in use
	 * @return
	 */
	public static TouchPointer getPointer()
	{
		TouchPointer p = null;
		//Log.d(TAG, "size " + activePointers.size());
		for(int i = 0; i < activePointers.size(); i++)
		{
			p = activePointers.get(activePointers.keyAt(i));
			if(p != null)
			{
				if(p.inUse == false)
				{
					return p;
				}
				else
				{
					p = null;
				}
			}
		}
		return p;
	}
	/**
	 * Returns a pointer (touch point) with the id
	 * @return
	 */
	public static TouchPointer getPointerAt(int id)
	{
		TouchPointer p = activePointers.get(id);
		return p;
	}
	public static boolean isDown()
	{
		return isDown;
	}
	public static float getX()
	{
		return x;
	}
	public static float getY()
	{
		return y;
	}
	/**
	 * The inner class that holds each point
	 * @author Johan
	 *
	 */
	public static class TouchPointer
	{
		private int id;
		private float x;
		private float y;
		
		private boolean inUse;

		public float getX()
		{
			return x;
		}
		public float getY()
		{
			return y;
		}
		public int getId()
		{
			return id;
		}
		public boolean getInUse()
		{
			return inUse;
		}
		public void inUse()
		{
			inUse = true;
		}
	}
}
