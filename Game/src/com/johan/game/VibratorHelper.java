package com.johan.game;

import android.content.Context;
import android.os.Vibrator;

/**
 * Helper class to help using vibration easier
 * @author Johan
 *
 */
public class VibratorHelper
{
	public static void makeVibration(int duration)
	{
		//Gets the Vibrator service from the system 
		Vibrator vibrator = (Vibrator)GameView.context().getSystemService(Context.VIBRATOR_SERVICE);
		vibrator.vibrate(duration);
	}
}
