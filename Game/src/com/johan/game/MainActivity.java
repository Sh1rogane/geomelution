package com.johan.game;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

/**
 * Main activity that fixes the window and adds the gameview
 * @author Johan
 *
 */
public class MainActivity extends Activity
{
	private GameView gameView;


	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		//Makes the screen fullscreen and keeps screen on
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

		
		//Everything below is used to activate "Immesive mode" on android 4.4+
		int currentApiVersion = android.os.Build.VERSION.SDK_INT;

		final int flags = View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
				| View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;

		// This work only for android 4.4+
		if(currentApiVersion >= 19)
		{
			getWindow().getDecorView().setSystemUiVisibility(flags);
			final View decorView = getWindow().getDecorView();
			decorView.setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener()
			{

				@Override
				public void onSystemUiVisibilityChange(int visibility)
				{
					if((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0)
					{
						decorView.setSystemUiVisibility(flags);
					}
				}
			});
		}

		gameView = new GameView(this);
		setContentView(gameView);
	}
	@Override
	protected void onPause()
	{
		super.onPause();
	}
	@Override
	protected void onResume()
	{
		super.onResume();
	}
	@Override
	protected void onDestroy()
	{
		super.onDestroy();
	}
	@Override
	protected void onStop()
	{
		super.onStop();

	}



}
