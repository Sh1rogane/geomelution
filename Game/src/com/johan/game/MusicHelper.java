package com.johan.game;

/**
 * Helper class to make playing music easier
 * @author Johan
 *
 */
public class MusicHelper
{
	private static int pauseTime = 0;
	public static void startMusic()
	{
		if(GameView.getMediaPlayer() != null)
		{
			GameView.getMediaPlayer().seekTo(0);
			GameView.getMediaPlayer().setLooping(true);
			GameView.getMediaPlayer().start();
		}
		
	}
	public static void resumeMusic()
	{
		if(GameView.getMediaPlayer() != null)
		{
			GameView.getMediaPlayer().seekTo(pauseTime);
			GameView.getMediaPlayer().setLooping(true);
			GameView.getMediaPlayer().start();
		}
	}
	public static void stopMusic()
	{
		if(GameView.getMediaPlayer() != null)
		{
			pauseTime = GameView.getMediaPlayer().getCurrentPosition();
			GameView.getMediaPlayer().pause();
		}
	}
}
